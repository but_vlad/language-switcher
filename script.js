var translation;

$(document).ready(function () {
    $.ajax({
        url: "language.json",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data, textStatus, jqXHR) {
            translation = data;
            $("#fraza1").html(data.textsen[0].text1)
            $("#fraza2").html(data.textsen[1].text2)
        }

    })
    console.log("da");
})

function changelanguage(language) {

    if (language == 'en') {
        $("#fraza1").html(translation.textsen[0].text1)
        $("#fraza2").html(translation.textsen[1].text2)
    } else {
        $("#fraza1").html(translation.textsfr[0].text1)
        $("#fraza2").html(translation.textsfr[1].text2)
    }

}